#!/usr/bin/env bash
#
# Bootstrap script that sets up dotfiles
#

#
# Default options:
# abort on error
set -o errexit
# exit on attempt to access unset variables
set -o nounset
# uncomment for debugging
# set -o xtrace

#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"


# Enable logging
source "${__DIR}/include/logging.sh"
# 7=debug, 0=emergency
LOG_LEVEL=7

# Ensure there is a .profile to work with
if [ ! -e "$HOME/.profile" ]; then
	info "No profile found, creating"
	touch "$HOME/.profile"
fi

# Check if the bootstrap script is being loaded from the profile
if grep -q "PROFILE_ROLES_ON_LOGIN" "${HOME}/.profile"; then
	debug "on-login loader already present, skipping"
else
	PROFILE_PATH="$HOME/.profile"
	info "Adding loader to ${PROFILE_PATH}"
	echo "" >> "$PROFILE_PATH"
	echo "# PROFILE_ROLES_ON_LOGIN" >> "$PROFILE_PATH"
	echo "# See dotfiles/Readme.md for more information" >> "$PROFILE_PATH"
	echo "source ${HOME}/dotfiles/on-login.sh" >> "$PROFILE_PATH"
fi

#
# Ensure that everything has been updated and synced
${__DIR}/update.sh

