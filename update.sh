#!/usr/bin/env bash
#
# Bootstrap script that sets up dotfiles
#

#
# Default options:
# abort on error
set -o errexit
# exit on attempt to access unset variables
set -o nounset
# uncomment for debugging
# set -o xtrace

#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"


# Enable logging
source "${__DIR}/include/logging.sh"
# 7=debug, 6=info, 0=emergency
LOG_LEVEL=6

info "Updating installed roles..."

ROLES=$("${__DIR}/get-roles")
debug "Roles: $ROLES"

function brew_install_if_not_present()
{
    local returnCode=0

    for packageName in $@
    do
        # To check installation status we can call brew list and check
        # return code
        set +o errexit
        brew list "$packageName" &> /dev/null
        returnCode=$?
        set -o errexit

        if [ $returnCode != 0 ]; then
            brew install "$packageName"
        fi
    done
}

function cask_install_if_not_present()
{
    local returnCode=0

    for packageName in $@
    do
        # To check installation status we can call brew cask list and check
        # return code
        set +o errexit
        brew cask list "$packageName" &> /dev/null
        returnCode=$?
        set -o errexit

        if [ $returnCode != 0 ]; then
            brew cask install "$packageName"
        fi
    done
}

#
# Update enabled roles

# Clear out all existing enabled roles, they will be re-added below
for ENABLED_ROLE in `ls "${__DIR}/roles-enabled/"`
do
	ENABLED_ROLE_DIR="${__DIR}/roles-enabled/${ENABLED_ROLE}"
	if [ -L "$ENABLED_ROLE_DIR" ]; then
		rm "${ENABLED_ROLE_DIR}"
	fi
done

# Process all enabled roles
for ROLE in $ROLES
do
	info "Role: $ROLE"
	ROLE_ROOT="${__DIR}/roles/${ROLE}"

	if [ ! -e "${ROLE_ROOT}" ]; then
		warning "Role root directory not found in ${ROLE_ROOT}"
		continue
	fi

	# Install homebrew taps
	HOMEBREW_TAPS_FILE="${ROLE_ROOT}/requirements/homebrew-taps.txt"
	if [[ $(which brew) && -e "$HOMEBREW_TAPS_FILE" ]]; then
		info "  > Installing homebrew taps"
		brew tap `cat "${HOMEBREW_TAPS_FILE}"` 2> /dev/null
	else
		debug "Brew not installed or no taps"
	fi

	# Perform automatic install of brew packages
	HOMEBREW_PACKAGES_FILE="${ROLE_ROOT}/requirements/homebrew-packages.txt"
	if [[ $(which brew) && -e "$HOMEBREW_PACKAGES_FILE" ]]; then
		info "  > Installing homebrew packages"
		brew_install_if_not_present `cat "${HOMEBREW_PACKAGES_FILE}"`
	else
		debug "Brew not installed or no packages"
	fi

	# Perform automatic install of cask packages
	CASK_PACKAGES_FILE="${ROLE_ROOT}/requirements/cask-packages.txt"
	if [[ $(which brew) && -e "$CASK_PACKAGES_FILE" ]]; then
		# Ensure cask is installed
		brew install caskroom/cask/brew-cask 2> /dev/null
		info "  > Installing cask packages"
		cask_install_if_not_present `cat "${CASK_PACKAGES_FILE}"` 2> /dev/null
	else
		debug "Brew not installed or no cask packages"
	fi

	# If present, run install script
	INSTALL_FILE="${ROLE_ROOT}/install.sh"
	if [ -e "$INSTALL_FILE" ]; then
		debug "  Running install file"
		"$INSTALL_FILE"
	else
		debug "  No install file found"
	fi

	# symlink role so that it is enabled
	info "  > Enabling role"
	ln -s "${ROLE_ROOT}" "${__DIR}"/roles-enabled
done






