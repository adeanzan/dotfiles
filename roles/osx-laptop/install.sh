#!/usr/bin/env bash
#
# Performs any steps necessary to install the role. This is only
# executed when an update is performed.
#

#
# Default options:
# abort on error
set -o errexit
# exit on attempt to access unset variables
set -o nounset
# uncomment for debugging
# set -o xtrace

#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"

#
# OS Configuration
# Mostly from: https://github.com/mathiasbynens/dotfiles/blob/master/.osx

# Trackpad: enable tap to click for this user and for the login screen
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

# Trackpad: map bottom right corner to right-click
# todo: seems to do something weird when tapping in the middle of the trackpad?
#defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
#defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
#defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
#defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true