#!/usr/bin/env bash
#
# Performs any steps necessary to install the role. This is only
# executed when an update is performed.
#

#
# Default options:
# abort on error
set -o errexit
# exit on attempt to access unset variables
set -o nounset
# uncomment for debugging
# set -o xtrace

#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"

# Import filesystem functions
source "${__DIR}/../../include/filesystem.sh"

#
# Link config files
CONFIG_DIR="${__DIR}/../../config"

# ssh client
mkdir -p "${HOME}/.ssh"
chmod 0700 "${HOME}/.ssh"
link_with_backup "${HOME}/.ssh/config" "${CONFIG_DIR}/ssh/config"

# vim
link_with_backup "${HOME}/.vimrc" "${CONFIG_DIR}/vim/vimrc"

# git
link_with_backup "${HOME}/.gitconfig" "${CONFIG_DIR}/git/gitconfig"
link_with_backup "${HOME}/.gitignore_global" "${CONFIG_DIR}/git/gitignore_global"