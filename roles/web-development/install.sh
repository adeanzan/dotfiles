#!/usr/bin/env bash
#
# Performs any steps necessary to install the role. This is only
# executed when an update is performed.
#

#
# Default options:
# abort on error
set -o errexit
# exit on attempt to access unset variables
set -o nounset
# uncomment for debugging
# set -o xtrace

#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"

# Import logging
source "${__DIR}/../../include/logging.sh"

# Add file associations
duti -v "${__DIR}/config/duti.conf"


#
# Custom dns resolution
#
# Configure dnsmasq (only runs once)
if [ ! -e "/usr/local/etc/dnsmasq.conf" ]; then
    info "Performing initial dnsmasq configuration (may require sudo)"
    cp /usr/local/opt/dnsmasq/dnsmasq.conf.example /usr/local/etc/dnsmasq.conf

    sudo cp -fv /usr/local/opt/dnsmasq/*.plist /Library/LaunchDaemons
    sudo chown root /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist

    sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
fi

# 
# Ensure /etc/resolver/localdev exists
if [ ! -e "/etc/resolver/localdev" ]; then
    info "Creating /etc/resolver/localdev (may require sudo)"
    sudo mkdir -p /etc/resolver
    sudo tee /etc/resolver/localdev >/dev/null <<EOF
nameserver 127.0.0.1
EOF
fi

#
# Install php5
if [ ! -e "/usr/local/php5" ]; then
    info "Installing PHP 5.6"
    curl -s http://php-osx.liip.ch/install.sh | bash -s 5.6
fi

