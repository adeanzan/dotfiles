#!/usr/bin/env bash
#
# Performs any steps necessary to install the role. This is only
# executed when an update is performed.
#

#
# Default options:
# abort on error
set -o errexit
# exit on attempt to access unset variables
set -o nounset
# uncomment for debugging
# set -o xtrace

#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__FILE="${__DIR}/$(basename "${BASH_SOURCE[0]}")"

#
# Imports

# Logging
source "${__DIR}/../../include/logging.sh"
# Import utilities for working with os x
source "${__DIR}/../../include/osx.sh"

#
# Replace sublime text config files with symlinks
# Sublime user config dir
SUBL_USER_CONFIG_DIR="${HOME}/Library/Application Support/Sublime Text 3/Packages/User"
mkdir -p "$SUBL_USER_CONFIG_DIR"

# General settings
rm -f "$SUBL_USER_CONFIG_DIR/Preferences.sublime-settings"
ln -s "${__DIR}/config/Preferences.sublime-settings" "$SUBL_USER_CONFIG_DIR/Preferences.sublime-settings"

# Key bindings
rm -f "$SUBL_USER_CONFIG_DIR/Default (OSX).sublime-keymap"
ln -s "${__DIR}/config/Default (OSX).sublime-keymap" "$SUBL_USER_CONFIG_DIR/Default (OSX).sublime-keymap"


#
# OS Configuration
# Mostly from: https://github.com/mathiasbynens/dotfiles/blob/master/.osx

# Disable transparency in the menu bar and elsewhere on Yosemite
info_if $(defaults_write_if_changed com.apple.universalaccess reduceTransparency -bool true)

# Always show scrollbars
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"
# Possible values: `WhenScrolling`, `Automatic` and `Always`

# Increase window resize speed for Cocoa applications
defaults write NSGlobalDomain NSWindowResizeTime -float 0.001

# Expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

# Expand print panel by default
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

# Save to disk (not to iCloud) by default
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

# Reveal IP address, hostname, OS version, etc. when clicking the clock
# in the login window
info_if $(defaults_write_if_changed_sudo /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName)

# Disable smart quotes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

# Disable smart dashes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

# Enable full keyboard access for all controls
# (e.g. enable Tab in modal dialogs)
defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

# Keyboard repeat rate and repeat delay
defaults write NSGlobalDomain InitialKeyRepeat -int 12 # Lowest via UI is 15
defaults write NSGlobalDomain KeyRepeat -int 2 # Lowest via UI is 2

# Change minimize/maximize window effect
defaults write com.apple.dock mineffect -string "scale"

# Speed up Mission Control animations
defaults write com.apple.dock expose-animation-duration -float 0.1

# Disable Dashboard
defaults write com.apple.dashboard mcx-disabled -bool true

# Don’t group windows by application in Mission Control
# (i.e. use the old Exposé behavior instead)
defaults write com.apple.dock expose-group-by-app -bool false

# Don’t show Dashboard as a Space
defaults write com.apple.dock dashboard-in-overlay -bool true

# Don’t automatically rearrange Spaces based on most recent use
defaults write com.apple.dock mru-spaces -bool false

# Remove the auto-hiding Dock delay
defaults write com.apple.dock autohide-delay -float 10
# Remove the animation when hiding/showing the Dock
defaults write com.apple.dock autohide-time-modifier -float 0

# Automatically hide and show the Dock
defaults write com.apple.dock autohide -bool true

