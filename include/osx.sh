#
# Utility functions for interacting with OS X
#


function osx_build_defaults_write_command()
{
  local numArgs="$#"
  local domain="$1"; shift
  local key="$1"; shift

  # 3 arguments, data type is omitted and default to string
  if [ "$numArgs" = 3 ]; then
    local dataType="-string"
    local desiredValue="$1"; shift
  fi
  # 4 arguments, datatype is specified
  if [ "$numArgs" = 4 ]; then
    local dataType="$1"; shift
    local desiredValue="$1"; shift
  fi
  
  local valueMatches=0
  local isBoolean=0

  local currentValue="$(defaults read $domain $key)"

  # Compare booleans
  if [[ "$desiredValue" == "true" && "$currentValue" == "1" ]]; then
    valueMatches="1"
    isBoolean="1"
  fi
  if [[ "$desiredValue" == "false" && "$currentValue" == "0" ]]; then
    valueMatches="1"
    isBoolean="1"
  fi

  # If it's not a boolean, do a normal comparison
  if [[ "$isBoolean" != "1" && "$currentValue" == "$desiredValue" ]]; then
    valueMatches="1"
  fi

  # Exit if value matches
  if [[ "$valueMatches" == "1" ]]; then
    return 
  fi

  echo defaults write "$domain" "$key" "$dataType" "$desiredValue"
}

function defaults_write_if_changed()
{
  local command=$(osx_build_defaults_write_command "$@")

  # todo: find good way to move argument parsing into a function
  local numArgs="$#"
  local domain="$1"; shift
  local key="$1"; shift

  # 3 arguments, data type is omitted and default to string
  if [ "$numArgs" = 3 ]; then
    local dataType="-string"
    local desiredValue="$1"; shift
  fi
  # 4 arguments, datatype is specified
  if [ "$numArgs" = 4 ]; then
    local dataType="$1"; shift
    local desiredValue="$1"; shift
  fi

  if [ "$command" != "" ]; then
    $command
    echo "Set $domain $key to $desiredValue"
  fi
}

function defaults_write_if_changed_sudo()
{
  local command=$(osx_build_defaults_write_command "$@")

  # todo: find good way to move argument parsing into a function
  local numArgs="$#"
  local domain="$1"; shift
  local key="$1"; shift

  # 3 arguments, data type is omitted and default to string
  if [ "$numArgs" = 3 ]; then
    local dataType="-string"
    local desiredValue="$1"; shift
  fi
  # 4 arguments, datatype is specified
  if [ "$numArgs" = 4 ]; then
    local dataType="$1"; shift
    local desiredValue="$1"; shift
  fi

  if [ "$command" != "" ]; then
    sudo $command
    echo "Set $domain $key to $desiredValue"
  fi
}


