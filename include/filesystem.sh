#
# Filesystem Functions
#
# Utilities for finding and working with files
#

#
# Recursively finds a path by checking parent directories
# until the root directory is found or the specified path
# is found
#
# @param targetPath - Path to search for, ex: bin/project-script.sh
#
# @return fully-qualified path to the file, if found or an empty string if not found
#
function find_in_parents ()
{
  local targetPath="$1"; shift
  local originalDir=`pwd`

  until [ `pwd` = '/' ]; do
    if [ -e "$targetPath" ]; then
      echo "$(pwd)/${targetPath}"
      return
    else
      cd ..
    fi
  done
}

function backup_file ()
{
  local filePath="$1"; shift

  local backupDirectory="$(dirname $filePath)"
  local backupFilename="$(basename $filePath)"
  local backupPath=""

  # Verify source file exists
  if [ ! -e "$filePath" ]; then
    echo "[backup_file] File does not exist: $filePath"
    exit 1
  fi  

  # Try to just do the .bak extension
  backupPath="${backupDirectory}/${backupFilename}.bak"

  # Already a .bak file, append epoch timestamp
  # todo: a non-terrible way to find a prettier unused filename
  if [ -e "$backupPath" ]; then
    backupPath="${backupDirectory}/${backupFilename}.bak-"`date "+%s"`
  fi
  
  cp "$filePath" "$backupPath"
}

#
# Creates a symlink in location $target that links to $sourceFile
#
# If $target already exists, it is backed up
# 
function link_with_backup ()
{
  local target="$1"; shift
  local sourceFile="$1"; shift

  # Verify source file exists
  if [ ! -e "$sourceFile" ]; then
    echo "[link_with_backup] Source file does not exist: $sourceFile"
    exit 1
  fi

  # If target exists and is not a symlink, back it up
  if [[ -e "$target" && ! -h "$target" ]]; then
    backup_file "$target"
    rm "$target"
  fi

  ln -sf "$sourceFile" "$target"
}






