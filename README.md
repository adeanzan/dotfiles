Dotfile Roles
-----------------------------

A role-based method for managing bash dotfiles, applications, and configuration.

## Installation

    $ git clone git@bitbucket.org:adeanzan/dotfiles.git
    $ dotfiles/bootstrap.sh
    $ . .profile