#!/usr/bin/env bash
#
# Runs on every login
#


#
# Magic variables
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

DOTFILES_ROOT="${__DIR}"
ROLES_ROOT="${__DIR}/roles"
ENABLED_ROLES_ROOT="${__DIR}/roles-enabled"

function add_bin_dir_to_path ()
{
	local roleRoot="${ROLES_ROOT}/${1}"; shift
	local roleBinDir="${roleRoot}/bin"

	export PATH="$PATH":"$roleBinDir"
}

function source_profile()
{
	local roleRoot="${ROLES_ROOT}/${1}"; shift
	local roleProfilePath="${roleRoot}/profile"

	if [ -e "$roleProfilePath" ]; then
		source "$roleProfilePath"
	fi
}

for ENABLED_ROLE in `ls "$ENABLED_ROLES_ROOT"`; do
	ROLE_ROOT="${ROLES_ROOT}/${ENABLED_ROLE}"
	
	# Add script's bin directory to path, if present
	add_bin_dir_to_path "$ENABLED_ROLE"

	# Execute role's profile
	source_profile "$ENABLED_ROLE"
done

# Cleanup
unset __DIR
unset ENABLED_ROLES_ROOT